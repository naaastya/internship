public class Human {
    private String firstName;
    private String lastName;
    private String middleName;
    private int age;
    private String gender;
    private String birthDate;

    public Human(String firstName, String lastName, String middleName, int age, String gender, String birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.age = age;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public boolean compare(Human human) {
        if (firstName.equals(human.firstName) && lastName.equals(human.lastName)
                && birthDate.equals(human.birthDate) && gender.equals(human.gender))
            return true;
        else return false;
    }
}
