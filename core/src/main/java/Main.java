import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Human human = new Human("zzzz", "zzzz", "zzzz", 14, "male", "25.05.2000");
        ArrayList<Human> humans = new ArrayList<>();
        humans.add(new Human("zzzz", "zzzz", "zzzz", 14, "male", "25.05.2000"));
        boolean isAnyOneTrue = humans.stream().anyMatch(human::compare);
        System.out.println(isAnyOneTrue);
    }

}
